###### 4DI : Cognome e nome <span style="padding-left:300px;">Data</span>

Data la seguente rete :

<img style="width:500px; margin:auto; display:block; padding-bottom:20px" src="./compito-router-1.png" alt="Rete di riferimento"/>


Sapendo che :
- Il router R1 è il punto di uscita verso Internet ed il suo "IP esterno" è il 79.47.179.139
- Il router R1 espone all'esterno 3 webserver disseminati nella rete attraverso le porte 80 (pc A), 8080 (pc B) e 8081 (pc C)
- Che la rete internamente utilizza gli indirizzi della fascia 172.16.0.0/12

Definire:
1. Gli Indirizzi IP delle reti di cui risulta composta lo schema in figura
1. Gli indirizzi IP dei pc A, B, C, D
1. Gli indirizzi IP delle interfacce di R1, R2, R3 e R4
1. Le tabelle di routing di R1, R2, R3 e R4 affinchè tutti i pc della intranet possano comunicare tra loro
1. La tabella del port forwarding del router R1

Produrre quindi una simulazione su <code>packet Tracer</code> che ne mostri il funzionamento